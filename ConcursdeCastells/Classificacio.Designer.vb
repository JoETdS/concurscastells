﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Classificacio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.classificacioBox = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'classificacioBox
        '
        Me.classificacioBox.BackColor = System.Drawing.SystemColors.InfoText
        Me.classificacioBox.Font = New System.Drawing.Font("Candara", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.classificacioBox.ForeColor = System.Drawing.SystemColors.Menu
        Me.classificacioBox.FormattingEnabled = True
        Me.classificacioBox.ItemHeight = 64
        Me.classificacioBox.Items.AddRange(New Object() {"Primera Colla", "Segona Colla", "Tercera Colla", "Quarta Colla", "Cinquena Colla"})
        Me.classificacioBox.Location = New System.Drawing.Point(0, 0)
        Me.classificacioBox.Name = "classificacioBox"
        Me.classificacioBox.Size = New System.Drawing.Size(800, 388)
        Me.classificacioBox.TabIndex = 0
        '
        'Classificacio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(808, 335)
        Me.Controls.Add(Me.classificacioBox)
        Me.Name = "Classificacio"
        Me.Text = "Classificacio"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents classificacioBox As ListBox
End Class
