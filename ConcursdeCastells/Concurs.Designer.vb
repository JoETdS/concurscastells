﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Concurs
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.nomColla = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pos_Descarregat = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pos_Carregat = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.post_Intent = New System.Windows.Forms.Label()
        Me.nomCastell = New System.Windows.Forms.Label()
        Me.fotoEscut = New System.Windows.Forms.PictureBox()
        CType(Me.fotoEscut, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'nomColla
        '
        Me.nomColla.AutoSize = True
        Me.nomColla.Font = New System.Drawing.Font("Javanese Text", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nomColla.ForeColor = System.Drawing.SystemColors.Control
        Me.nomColla.Location = New System.Drawing.Point(12, -2)
        Me.nomColla.Name = "nomColla"
        Me.nomColla.Size = New System.Drawing.Size(452, 145)
        Me.nomColla.TabIndex = 8
        Me.nomColla.Text = "NOM COLLA"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Javanese Text", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(-1, 223)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(307, 73)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "DESCARREGUEN"
        '
        'pos_Descarregat
        '
        Me.pos_Descarregat.AutoSize = True
        Me.pos_Descarregat.Font = New System.Drawing.Font("Javanese Text", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pos_Descarregat.ForeColor = System.Drawing.SystemColors.Control
        Me.pos_Descarregat.Location = New System.Drawing.Point(61, 278)
        Me.pos_Descarregat.Name = "pos_Descarregat"
        Me.pos_Descarregat.Size = New System.Drawing.Size(187, 145)
        Me.pos_Descarregat.TabIndex = 10
        Me.pos_Descarregat.Text = "POS"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Javanese Text", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(304, 223)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(241, 73)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "CARREGUEN"
        '
        'pos_Carregat
        '
        Me.pos_Carregat.AutoSize = True
        Me.pos_Carregat.Font = New System.Drawing.Font("Javanese Text", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pos_Carregat.ForeColor = System.Drawing.SystemColors.Control
        Me.pos_Carregat.Location = New System.Drawing.Point(329, 278)
        Me.pos_Carregat.Name = "pos_Carregat"
        Me.pos_Carregat.Size = New System.Drawing.Size(187, 145)
        Me.pos_Carregat.TabIndex = 12
        Me.pos_Carregat.Text = "POS"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Javanese Text", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(551, 223)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(266, 73)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "ACTUALMENT"
        '
        'post_Intent
        '
        Me.post_Intent.AutoSize = True
        Me.post_Intent.Font = New System.Drawing.Font("Javanese Text", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.post_Intent.ForeColor = System.Drawing.SystemColors.Control
        Me.post_Intent.Location = New System.Drawing.Point(601, 278)
        Me.post_Intent.Name = "post_Intent"
        Me.post_Intent.Size = New System.Drawing.Size(187, 145)
        Me.post_Intent.TabIndex = 14
        Me.post_Intent.Text = "POS"
        '
        'nomCastell
        '
        Me.nomCastell.AutoSize = True
        Me.nomCastell.Font = New System.Drawing.Font("Javanese Text", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nomCastell.ForeColor = System.Drawing.SystemColors.Control
        Me.nomCastell.Location = New System.Drawing.Point(38, 89)
        Me.nomCastell.Name = "nomCastell"
        Me.nomCastell.Size = New System.Drawing.Size(351, 145)
        Me.nomCastell.TabIndex = 15
        Me.nomCastell.Text = "CASTELL"
        '
        'fotoEscut
        '
        Me.fotoEscut.Location = New System.Drawing.Point(506, 29)
        Me.fotoEscut.Name = "fotoEscut"
        Me.fotoEscut.Size = New System.Drawing.Size(254, 160)
        Me.fotoEscut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.fotoEscut.TabIndex = 16
        Me.fotoEscut.TabStop = False
        '
        'Concurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlText
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.fotoEscut)
        Me.Controls.Add(Me.nomCastell)
        Me.Controls.Add(Me.post_Intent)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.pos_Carregat)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.pos_Descarregat)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.nomColla)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Name = "Concurs"
        Me.Text = "Concurs de Castells"
        CType(Me.fotoEscut, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents nomColla As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents pos_Descarregat As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents pos_Carregat As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents post_Intent As Label
    Friend WithEvents nomCastell As Label
    Friend WithEvents fotoEscut As PictureBox
End Class
