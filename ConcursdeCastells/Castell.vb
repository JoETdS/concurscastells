﻿Public Class Castell

    Private nom As String
    Private carregat As Integer
    Private descarregat As Integer

    'Constructor
    Public Sub New(ByVal nom As String, ByVal carregat As Integer, ByVal descarregat As Integer)
        Me.nom = nom
        Me.carregat = carregat
        Me.descarregat = descarregat
    End Sub

    'Aconseguir puntuació del castell
    Public Function getPunts(ByVal descarregat As Boolean) As Integer
        If descarregat = True Then
            Return Me.descarregat
        Else
            Return Me.carregat
        End If
    End Function

    Public Function getNom() As String
        Return Me.nom
    End Function

End Class
