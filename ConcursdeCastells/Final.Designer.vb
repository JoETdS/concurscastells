﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Final
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.resultat2a7 = New System.Windows.Forms.ListBox()
        Me.resultat8a12 = New System.Windows.Forms.ListBox()
        Me.guanyador = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'resultat2a7
        '
        Me.resultat2a7.BackColor = System.Drawing.SystemColors.InfoText
        Me.resultat2a7.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.resultat2a7.ForeColor = System.Drawing.SystemColors.Info
        Me.resultat2a7.FormattingEnabled = True
        Me.resultat2a7.ItemHeight = 37
        Me.resultat2a7.Items.AddRange(New Object() {"El nom de la colla - punts"})
        Me.resultat2a7.Location = New System.Drawing.Point(0, 52)
        Me.resultat2a7.Name = "resultat2a7"
        Me.resultat2a7.Size = New System.Drawing.Size(402, 411)
        Me.resultat2a7.TabIndex = 16
        '
        'resultat8a12
        '
        Me.resultat8a12.BackColor = System.Drawing.SystemColors.InfoText
        Me.resultat8a12.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.resultat8a12.ForeColor = System.Drawing.SystemColors.Info
        Me.resultat8a12.FormattingEnabled = True
        Me.resultat8a12.ItemHeight = 37
        Me.resultat8a12.Items.AddRange(New Object() {"El nom de la colla - punts"})
        Me.resultat8a12.Location = New System.Drawing.Point(408, 52)
        Me.resultat8a12.Name = "resultat8a12"
        Me.resultat8a12.Size = New System.Drawing.Size(395, 411)
        Me.resultat8a12.TabIndex = 17
        '
        'guanyador
        '
        Me.guanyador.BackColor = System.Drawing.SystemColors.InfoText
        Me.guanyador.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guanyador.ForeColor = System.Drawing.Color.Lime
        Me.guanyador.FormattingEnabled = True
        Me.guanyador.ItemHeight = 42
        Me.guanyador.Items.AddRange(New Object() {"El nom de la colla - punts"})
        Me.guanyador.Location = New System.Drawing.Point(0, 0)
        Me.guanyador.Name = "guanyador"
        Me.guanyador.Size = New System.Drawing.Size(803, 46)
        Me.guanyador.TabIndex = 18
        '
        'Final
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InfoText
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.guanyador)
        Me.Controls.Add(Me.resultat8a12)
        Me.Controls.Add(Me.resultat2a7)
        Me.Name = "Final"
        Me.Text = "Final"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents resultat2a7 As ListBox
    Friend WithEvents resultat8a12 As ListBox
    Friend WithEvents guanyador As ListBox
End Class
