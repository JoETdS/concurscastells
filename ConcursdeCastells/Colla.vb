﻿Public Class Colla

    Private nom As String
    Private puntuacio(4) As Integer
    Private puntsTotals As Integer
    Private registreCastells(4) As Castell
    Private escut As String

    'Constructor buit
    Public Sub New()
    End Sub

    'Constructor per nom
    Public Sub New(ByVal nom As String, ByVal escut As String)
        Me.nom = nom
        Me.escut = escut
    End Sub

    Public Function getNom() As String
        Return Me.nom
    End Function

    Public Sub setPuntuacio(ByVal ronda As Integer, ByVal puntuacio As Integer)
        Me.puntuacio(ronda) = puntuacio
        actualitzarPuntuacio()
    End Sub

    Public Function getPuntuacioTotal() As Integer
        Return Me.puntsTotals
    End Function

    Public Sub actualitzarPuntuacio()
        Dim aux(4) As Integer
        Array.Copy(puntuacio, aux, 4)
        Array.Sort(aux)
        Me.puntsTotals = aux(4) + aux(3) + aux(2)
    End Sub

    Public Sub afegirCastell(ByVal ronda As Integer, ByVal castell As Castell)
        registreCastells(ronda) = castell
    End Sub

    Public Function getActuacio(ByVal ronda) As String
        If registreCastells(ronda) IsNot Nothing Then
            Return Me.registreCastells(ronda).getNom() + " - " + Convert.ToString(Me.puntuacio(ronda))
        Else
            Return Nothing
        End If

    End Function

    Friend Function getEsctut() As String
        Return Me.escut
    End Function
End Class
