﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ControlColles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.botoAcceptar = New System.Windows.Forms.Button()
        Me.BotoEnrere = New System.Windows.Forms.Button()
        Me.nomColla = New System.Windows.Forms.Label()
        Me.llistaCastells = New System.Windows.Forms.CheckedListBox()
        Me.boto_carregat = New System.Windows.Forms.Button()
        Me.boto_descarregat = New System.Windows.Forms.Button()
        Me.boto_intent = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.escutColla = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.actuacioBox = New System.Windows.Forms.ListBox()
        CType(Me.escutColla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nom Colla:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(189, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Grup 5:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(84, 210)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Castells:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(189, 158)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Grup 6:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(189, 248)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Grup 7:"
        '
        'botoAcceptar
        '
        Me.botoAcceptar.Location = New System.Drawing.Point(379, 158)
        Me.botoAcceptar.Name = "botoAcceptar"
        Me.botoAcceptar.Size = New System.Drawing.Size(144, 78)
        Me.botoAcceptar.TabIndex = 1
        Me.botoAcceptar.Text = "Acceptar"
        Me.botoAcceptar.UseVisualStyleBackColor = True
        '
        'BotoEnrere
        '
        Me.BotoEnrere.Location = New System.Drawing.Point(30, 328)
        Me.BotoEnrere.Name = "BotoEnrere"
        Me.BotoEnrere.Size = New System.Drawing.Size(191, 83)
        Me.BotoEnrere.TabIndex = 2
        Me.BotoEnrere.Text = "Tornar enrere"
        Me.BotoEnrere.UseVisualStyleBackColor = True
        '
        'nomColla
        '
        Me.nomColla.AutoSize = True
        Me.nomColla.Location = New System.Drawing.Point(127, 36)
        Me.nomColla.Name = "nomColla"
        Me.nomColla.Size = New System.Drawing.Size(69, 13)
        Me.nomColla.TabIndex = 7
        Me.nomColla.Text = "NOM COLLA"
        '
        'llistaCastells
        '
        Me.llistaCastells.FormattingEnabled = True
        Me.llistaCastells.Location = New System.Drawing.Point(258, 127)
        Me.llistaCastells.Name = "llistaCastells"
        Me.llistaCastells.Size = New System.Drawing.Size(120, 304)
        Me.llistaCastells.TabIndex = 8
        '
        'boto_carregat
        '
        Me.boto_carregat.Location = New System.Drawing.Point(379, 270)
        Me.boto_carregat.Name = "boto_carregat"
        Me.boto_carregat.Size = New System.Drawing.Size(144, 50)
        Me.boto_carregat.TabIndex = 9
        Me.boto_carregat.Text = "Carregat"
        Me.boto_carregat.UseVisualStyleBackColor = True
        '
        'boto_descarregat
        '
        Me.boto_descarregat.Location = New System.Drawing.Point(379, 326)
        Me.boto_descarregat.Name = "boto_descarregat"
        Me.boto_descarregat.Size = New System.Drawing.Size(144, 50)
        Me.boto_descarregat.TabIndex = 10
        Me.boto_descarregat.Text = "Descarregat"
        Me.boto_descarregat.UseVisualStyleBackColor = True
        '
        'boto_intent
        '
        Me.boto_intent.Location = New System.Drawing.Point(379, 382)
        Me.boto_intent.Name = "boto_intent"
        Me.boto_intent.Size = New System.Drawing.Size(144, 50)
        Me.boto_intent.TabIndex = 11
        Me.boto_intent.Text = "Intent"
        Me.boto_intent.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(292, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Escut:"
        '
        'escutColla
        '
        Me.escutColla.Location = New System.Drawing.Point(335, 1)
        Me.escutColla.Name = "escutColla"
        Me.escutColla.Size = New System.Drawing.Size(188, 120)
        Me.escutColla.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.escutColla.TabIndex = 13
        Me.escutColla.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(40, 55)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Actuació"
        '
        'actuacioBox
        '
        Me.actuacioBox.FormattingEnabled = True
        Me.actuacioBox.Location = New System.Drawing.Point(95, 55)
        Me.actuacioBox.Name = "actuacioBox"
        Me.actuacioBox.Size = New System.Drawing.Size(88, 95)
        Me.actuacioBox.TabIndex = 15
        '
        'ControlColles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 450)
        Me.Controls.Add(Me.actuacioBox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.escutColla)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.boto_intent)
        Me.Controls.Add(Me.boto_descarregat)
        Me.Controls.Add(Me.boto_carregat)
        Me.Controls.Add(Me.llistaCastells)
        Me.Controls.Add(Me.nomColla)
        Me.Controls.Add(Me.BotoEnrere)
        Me.Controls.Add(Me.botoAcceptar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "ControlColles"
        Me.Text = "Control"
        CType(Me.escutColla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents botoAcceptar As Button
    Friend WithEvents BotoEnrere As Button
    Friend WithEvents nomColla As Label
    Friend WithEvents llistaCastells As CheckedListBox
    Friend WithEvents boto_carregat As Button
    Friend WithEvents boto_descarregat As Button
    Friend WithEvents boto_intent As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents escutColla As PictureBox
    Friend WithEvents Label7 As Label
    Friend WithEvents actuacioBox As ListBox
End Class
