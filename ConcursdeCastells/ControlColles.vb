﻿Imports ConcursdeCastells

Public Class ControlColles

    Private Colles(11) As Colla
    Private Castells(19) As Castell
    Private Ronda As Integer
    Private IndexCollaActuant As Integer
    Private CastellActual As Castell

    '----------------------------------------------------------------------------------------------------------------------
    'Desenvolupament de l'aplicació
    '----------------------------------------------------------------------------------------------------------------------

    'Inici del programa
    'Inicialització 
    Private Sub ControlColles_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InicialitzarContadors()
        AmagarBotonsCastells()
        MostrarBotonsAvançar()
        InicialitzarCastells()
        MostarLlistatDeCastells()
        InicialitzarColles()
        CanviColla(1)
    End Sub
    'Canvi de colla
    Private Sub CanviColla(ByVal seguent As Integer)
        Me.IndexCollaActuant = Me.IndexCollaActuant + seguent
        ComprovarRonda()
        ActualitzarColla()
        MostrarBotonsAvançar()
        AmagarBotonsCastells()
        AmagarConcurs()
        ActualitzarRanking()
        NetejarSeleccioCastell()
    End Sub

    Private Sub FinalConcurs()
        AmagarTot()
        MostrarFinal()
        ResultatFinal()
    End Sub

    '----------------------------------------------------------------------------------------------------------------------
    'Inicialització del contingut
    '----------------------------------------------------------------------------------------------------------------------

    'Iniciem els contadors
    Private Sub InicialitzarContadors()
        Me.Ronda = 0
        Me.IndexCollaActuant = -1
    End Sub

    'Creem totes les colles
    Private Sub InicialitzarColles()
        Dim ruta As String = "C:\Users\alumne\Pictures\"
        Colles(0) = New Colla("C. de Sabadell", ruta + "sabadell.jpeg")
        Colles(1) = New Colla("C Joves X. de Valls", ruta + "jovesValls.jpeg")
        Colles(2) = New Colla("C. de sants", ruta + "sants.jpeg")
        Colles(3) = New Colla("C. de Sant Cugat", ruta + "cugat.jpeg")
        Colles(4) = New Colla("C. de Vilafranca", ruta + "vilafranca.jpeg")
        Colles(5) = New Colla("C. de Mataró", ruta + "mataro.jpeg")
        Colles(6) = New Colla("C. de la Vila de Gràcia", ruta + "gracia.jpeg")
        Colles(7) = New Colla("C. Jove X. de Tarragona", ruta + "joveTarragona.jpeg")
        Colles(8) = New Colla("X. de Tarragona", ruta + "tarragona.jpeg")
        Colles(9) = New Colla("C. de Salt", ruta + "salt.jpeg")
        Colles(10) = New Colla("C. Vella X. de Valls", ruta + "vellaValls.jpeg")
        Colles(11) = New Colla("C. de Barcelona", ruta + "barcelona.jpeg")

    End Sub

    'Creem tots els castells
    Private Sub InicialitzarCastells()

        Castells(0) = New Castell("4 de 9 f", 1270, 1460)
        Castells(1) = New Castell("3 de 9 f", 1335, 1530)
        Castells(2) = New Castell("9 de 8", 1665, 1915)
        Castells(3) = New Castell("3 de 8 ps", 1825, 2010)
        Castells(4) = New Castell("2 de 9 fm", 1835, 2110)
        Castells(5) = New Castell("P de 8 fm", 1925, 2210)
        Castells(6) = New Castell("7 de 9 f", 2020, 2320)
        Castells(7) = New Castell("5 de 9 f ", 2090, 2400)
        Castells(8) = New Castell("4 de 9 fa", 2250, 2475)
        Castells(9) = New Castell("3 de 9 fa", 2315, 2555)
        Castells(10) = New Castell("3 de 10 fm", 2775, 3195)
        Castells(11) = New Castell("4 de 10 fm", 2870, 3300)
        Castells(12) = New Castell("4 de 9 sf", 2680, 3405)
        Castells(13) = New Castell("2 de 8 sf", 2765, 3510)
        Castells(14) = New Castell("9 de 9 f", 3190, 3670)
        Castells(15) = New Castell("2 de 9 sm", 2915, 3705)
        Castells(16) = New Castell("2 de 10 fmp", 3370, 3870)
        Castells(17) = New Castell("P de 9 fmp", 3480, 4000)
        Castells(18) = New Castell("3 de 9 sf", 3250, 4130)
        Castells(19) = New Castell("4 de 10 sm", 3350, 4260)


    End Sub

    '----------------------------------------------------------------------------------------------------------------------
    'Interacció amb botons
    '----------------------------------------------------------------------------------------------------------------------

    'Accions en el moment de premer acceptar
    Private Sub botoAcceptar_Click(sender As Object, e As EventArgs) Handles botoAcceptar.Click
        AmagarClassificacio()
        Me.CastellActual = getCastellByName(llistaCastells.Items(llistaCastells.CheckedIndices(0)).ToString)
        ActualitzarPantallaActuacio()
        MostrarConcurs()
        MostrarBotonsCastells()
        AmagarBotonsAvançar()
    End Sub

    'Accions dels botons del resultat del castell
    Private Sub boto_carregat_Click(sender As Object, e As EventArgs) Handles boto_carregat.Click
        AfegirPuntuacio(CastellActual.getPunts(False))
        CanviColla(1)
    End Sub

    Private Sub boto_descarregat_Click(sender As Object, e As EventArgs) Handles boto_descarregat.Click
        AfegirPuntuacio(CastellActual.getPunts(True))
        CanviColla(1)
    End Sub

    Private Sub boto_intent_Click(sender As Object, e As EventArgs) Handles boto_intent.Click
        AfegirPuntuacio(0)
        CanviColla(1)
    End Sub

    Private Sub botoEnrere_Click(sender As Object, e As EventArgs) Handles BotoEnrere.Click
        AfegirPuntuacio(0)
        CanviColla(-1)
    End Sub

    '----------------------------------------------------------------------------------------------------------------------
    'Altres pantalles
    '----------------------------------------------------------------------------------------------------------------------

    'Actualtizar la informació de la colla que està actuant en la pantalla pel públic
    Private Sub ActualitzarPantallaActuacio()
        Concurs.nomCastell.Text = Me.CastellActual.getNom()
        Concurs.nomColla.Text = Me.Colles(IndexCollaActuant).getNom()
        Concurs.fotoEscut.ImageLocation = Me.Colles(IndexCollaActuant).getEsctut()
        PossibleClassificacio()
    End Sub

    Private Sub AmagarConcurs()
        Concurs.Visible = False
    End Sub

    Private Sub MostrarConcurs()
        Concurs.Visible = True
    End Sub

    Private Sub MostrarClassificacio()
        Classificacio.Visible = True
    End Sub

    Private Sub AmagarClassificacio()
        Classificacio.Visible = False
    End Sub

    Private Sub AmagarTot()
        AmagarConcurs()
        AmagarClassificacio()
    End Sub

    Private Sub MostrarFinal()
        Final.Visible = True
    End Sub


    '----------------------------------------------------------------------------------------------------------------------
    'Funcions auxiliars
    '----------------------------------------------------------------------------------------------------------------------

    Private Sub MostarLlistatDeCastells()
        For i = 0 To Castells.Length - 1
            llistaCastells.Items.Add(Castells(i).getNom)
        Next
    End Sub

    Private Sub CanviarNomColla()
        nomColla.Text = Colles(Me.IndexCollaActuant).getNom
        escutColla.ImageLocation = Colles(Me.IndexCollaActuant).getEsctut
    End Sub

    Private Sub MostrarActuacioCollaActual()
        actuacioBox.Items.Clear()
        For i = 0 To 4
            If Colles(IndexCollaActuant).getActuacio(i) IsNot Nothing Then
                actuacioBox.Items.Add(Colles(IndexCollaActuant).getActuacio(i))
            End If
        Next
    End Sub

    Private Sub MostrarBotonsCastells()
        boto_carregat.Visible = True
        boto_descarregat.Visible = True
        boto_intent.Visible = True
    End Sub

    Private Sub AmagarBotonsCastells()
        boto_carregat.Visible = False
        boto_descarregat.Visible = False
        boto_intent.Visible = False
    End Sub

    Private Sub MostrarBotonsAvançar()
        botoAcceptar.Visible = True
    End Sub

    Private Sub AmagarBotonsAvançar()
        botoAcceptar.Visible = False
    End Sub

    'Aconseguir una instància de l'objecte castell pel seu nom
    Private Function getCastellByName(ByVal nomCastell As String) As Castell
        For index = 0 To Castells.Length - 1
            If Castells(index).getNom() = nomCastell Then
                Return Castells(index)
            End If
        Next
        Return Nothing
    End Function

    Private Sub ActualitzarColla()
        CanviarNomColla()
        MostrarActuacioCollaActual()
    End Sub

    Private Sub NetejarSeleccioCastell()
        For i As Integer = 0 To llistaCastells.Items.Count - 1
            llistaCastells.SetItemChecked(i, False)
        Next
        llistaCastells.ClearSelected()
    End Sub

    Private Sub ComprovarRonda()
        If Me.IndexCollaActuant = 12 Then
            Me.IndexCollaActuant = 0
            Me.Ronda = Me.Ronda + 1
        ElseIf Me.IndexCollaActuant = -1 Then
            Me.IndexCollaActuant = 11
            Me.Ronda = Me.Ronda - 1
        End If
        If Me.Ronda = 5 Then
            FinalConcurs()
        End If
    End Sub

    Private Sub ActualitzarRanking()
        ActualitzarClassificacio(Colles.OrderByDescending(Function(c) c.getPuntuacioTotal).ToArray(), 4)
        MostrarClassificacio()
    End Sub

    Private Sub AfegirPuntuacio(ByVal novaPuntuacio As Integer)
        Me.Colles(Me.IndexCollaActuant).setPuntuacio(Me.Ronda, novaPuntuacio)
        Me.Colles(Me.IndexCollaActuant).afegirCastell(Me.Ronda, Me.CastellActual)
    End Sub

    Private Sub ActualitzarClassificacio(ByVal colles() As Colla, ByVal limit As Integer)
        NetejarRanking()
        If limit = 4 Then
            For i = 0 To limit
                Classificacio.classificacioBox.Items.Add(colles(i).getNom() + " - " + Convert.ToString(colles(i).getPuntuacioTotal()))
            Next
        ElseIf limit = 11 Then
            Final.guanyador.Items.Add(colles(1).getNom() + " - " + Convert.ToString(colles(1).getPuntuacioTotal()))
            For i = 1 To 6
                Final.resultat2a7.Items.Add(colles(i).getNom() + " - " + Convert.ToString(colles(i).getPuntuacioTotal()))
            Next
            For i = 7 To 11
                Final.resultat8a12.Items.Add(colles(i).getNom() + " - " + Convert.ToString(colles(i).getPuntuacioTotal()))
            Next
        End If

    End Sub

    Private Sub PossibleClassificacio()
        Dim auxColles(11) As Colla
        Array.Copy(Me.Colles, auxColles, Me.Colles.Length)
        Dim auxColla = Me.Colles(IndexCollaActuant)
        'Cas de caure
        Concurs.post_Intent.Text = getPosicio(auxColla.getNom, auxColles).ToString
        'Cas de descarregar
        auxColla.setPuntuacio(Me.Ronda, Me.CastellActual.getPunts(True))
        auxColles(IndexCollaActuant) = auxColla
        auxColles = auxColles.OrderByDescending(Function(c) c.getPuntuacioTotal).ToArray()
        Concurs.pos_Descarregat.Text = getPosicio(auxColla.getNom, auxColles).ToString
        'Cas de carregar
        auxColla.setPuntuacio(Me.Ronda, Me.CastellActual.getPunts(False))
        auxColles(IndexCollaActuant) = auxColla
        auxColles = auxColles.OrderByDescending(Function(c) c.getPuntuacioTotal).ToArray()
        Concurs.pos_Carregat.Text = getPosicio(auxColla.getNom, auxColles).ToString
    End Sub

    Private Function getPosicio(ByVal collaNom As String, ByVal auxColles() As Colla) As Integer
        For index = 0 To auxColles.Length
            If auxColles(index).getNom = collaNom Then
                Return index + 1
            End If
        Next
        Return 12
    End Function

    Private Sub NetejarRanking()
        Classificacio.classificacioBox.Items.Clear()
    End Sub

    Private Sub ResultatFinal()
        ActualitzarClassificacio(Colles.OrderByDescending(Function(c) c.getPuntuacioTotal).ToArray(), 11)
    End Sub

End Class